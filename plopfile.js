/**
 * plopfile.js for UI Develoment with Riot.js and ES6 Javascript
 */
var path = require('path');
var glob = require('glob');
var join = path.join;
/**
 * export
 */
module.exports = function (plop) {
    /**
     * validate whether user input a name or not
     */
    var checkName = function (value)
    {
        if ((/.+/).test(value)) {
            return true;
        }
        return 'name is required';
    };

    /**
     * get the list of container components dynamically
     */
    var getContainerComponents = function(more)
    {
        var list = glob.sync(
            join(__dirname , 'src' , 'app' , 'components' , 'containers' , '**' , '*.tag')
        );
        if (more) {
            list.push(more);
        }
        return list;
    };

    /**
     * get file by extension
     */
    var getFileByExtension = function(ext)
    {
        return glob.sync(
            join(__dirname , 'src' , '**' , '*.' + ext)
        );
    };

    /**
     * handlebar doesn't support EQUALS so we need a helper to do it
     */
     plop.addHelper('isStateless' , function(type , options)
     {
         if (type === 'stateless') {
             return options.fn(this);
         }
     });

     /**
      * tag generator
      */
     plop.setGenerator('tag', {
         description: 'tag generator',
         prompts: [{
             type: 'list',
             name: 'type',
             message: 'What is the type of the new tag?',
             choices: [
                 'containers',
                 'stateless'
             ],
             default: 'stateless'
         },{
             type: 'input',
             name: 'name',
             message: 'What is the name of the new tag?',
             validate: checkName
         },{
             type: 'confirm',
			 name: 'addToApp',
			 message: 'Do you want to import this tag to app.tag?',
             when: function(answer)
             {
                 return answer.type === 'containers';
             }
         },{
             type: 'checkbox',
             name: 'addToContainer',
             message: 'Do you want to import this to the following container tags?',
             choices: getContainerComponents(),
             when: function(answer)
             {
                 return answer.type === 'stateless';
             }
         }],
		 actions: function(data)
         {
             // move the original actions here
             var actions = [{
                type: 'add',
                path: 'src/app/components/' + data.type + '/{{dashCase name}}.tag',
                templateFile: 'templates/tag.txt',
                abortOnFail: true
             },{
    			type: 'add',
    			path: 'test/app/components/' + data.type + '/{{dashCase name}}.js',
    			templateFile: 'templates/tag-test-spec.txt'
    		}];
            // now check the confirm answer
            if (data.addToApp) {
                actions.push({
                    type: 'modify',
                    path: join(__dirname , 'src' , 'app' , 'components' , 'app.tag'),
                    pattern: /(\/\* import tag \*\/)/gi,
                    template: '$1\nimport \'./' + data.type + '/{{dashCase name}}.tag\';'
                });
            }
            else if (data.addToContainer) {
                // we need to generate a dynamic pattern based on the tag name
                data.addToContainer.forEach(function(container)
                {
                    var tagName = path.basename(container , '.tag');
                    actions.push({
                        type: 'modify',
                        path: container,
                        pattern: new RegExp('(\<' + tagName + ')' , 'gi') ,
                        template: 'import \'../stateless/{{dashCase name}}.tag\';\n$1'
                    });
                });
            }
            return actions;
         }
	});
    /**
     * mixin generator
     */
     plop.setGenerator('mixin', {
    	 description: 'mixin generator',
    	 prompts: [{
    		 type: 'input',
    		 name: 'name',
    		 message: 'What is the name of the new mixn?',
    		 validate: checkName
    	 },{
    		 type: 'confirm',
    		 name: 'addToApp',
    		 message: 'Do you want to import this mixn to app.tag?'
    	 }],
    	 actions: function(data)
    	 {
    		 var actions = [{
    			type: 'add',
    			path: 'src/app/mixins/{{dashCase name}}.js',
    			templateFile: 'templates/mixin.txt',
    			abortOnFail: true
    		 },{
    			type: 'add',
    			path: 'test/app/mixins/{{dashCase name}}.js',
    			templateFile: 'templates/mixin-test-spec.txt'
    		}];
    		// now check the confirm answer
    		if (data.addToApp) {
    			actions.push({
    				type: 'modify',
    				path: join(__dirname , 'src' , 'app' , 'components' , 'app.tag'),
    				pattern: /(\/\* import mixin \*\/)/gi,
    				template: '$1\nimport {{properCase name}} from \'../mixins/{{dashCase name}}\';',
    				abortOnFail: true
    			});
                actions.push({
                    type: 'modify',
    				path: join(__dirname , 'src' , 'app' , 'components' , 'app.tag'),
    				pattern: /(\/\* loading our mixin \*\/)/gi,
    				template: '$1\n\t\triot.mixin(\'{{properCase name}}\', {{properCase name}});',
    				abortOnFail: true
                });
    		}
    		return actions;
    	 }
    });

    /**
     * A model generator
     */
    plop.setGenerator('model' , {
        description: 'Model generator',
        prompts: [{
            type: 'input',
            name: 'name',
            message: 'What is the name of the new model?',
            validate: checkName
        },{
            type: 'confirm',
            name: 'import',
            message: 'Do you want to import it to a container component?'
        },{
            type: 'checkbox',
            name: 'containers',
            message: 'Select the container component to import',
            choices: getContainerComponents(join(__dirname , 'src' , 'app' , 'components' , 'app.tag')),
            when: function(answer)
            {
                return answer.import;
            }
        }],
        actions: function(data)
        {
            var actions = [{
                type: 'add',
                path: 'src/app/models/{{dashCase name}}.js',
                templateFile: 'templates/model.txt',
                abortOnFail: true
            },{
                type: 'add',
                path: 'test/app/models/{{dashCase name}}.js',
                templateFile: 'templates/model-spec.txt'
            }];
            if (data.containers) {
                data.containers.forEach(function(container)
                {
                    var tagName = path.basename(container , '.tag');
                    var importPath = tagName === 'app' ? '../' : '../../';
                    actions.push({
                        type: 'modify' ,
                        path: container ,
                        pattern: new RegExp('(\<' + tagName + ')' , 'gi') ,
                        template: 'import \'' + importPath + 'models/{{dashCase name}}.js\';\n$1'
                    });
                });
            }
            return actions;
        }
    });

     /**
      * A model generator , the different is the generator will not import it for you
      * because a Service is going to get import into another mixin or model
      */
     plop.setGenerator('service' , {
         description: 'Service generator',
         prompts: [{
             type: 'input',
             name: 'name',
             message: 'What is the name of the new service?',
             validate: checkName
         }],
         actions: [{
             type: 'add',
             path: 'src/app/services/{{dashCase name}}.js',
             templateFile: 'templates/service.txt',
             abortOnFail: true
         },{
             type: 'add',
             path: 'test/app/services/{{dashCase name}}.js',
             templateFile: 'templates/service-spec.txt'
         }]
     });

    /**
     * @TODO since our existing code does not have any test.spec files
     * then we need to adding them back retrospectively
     */
    plop.setGenerator('add-test', {
        description: 'Adding test files back restrospecitvely',
        prompts: [{
            type: 'list',
            name: 'type',
            message: 'What type of file you want to generate test spec?',
            choices: [
                'tag',
                'js'
            ],
            default: 'tag'
        }],
        actions: function(data) {
            var actions = [];
            var files = getFileByExtension(data.type);
            files.forEach(function(file)
            {
                console.log(file);
            });
            return actions;
        }
    });
};
