/**
 * Feed model
 */
import Storage from '../services/storage.js';


export default class Feed extends Storage {
	/**
	 * constructor call at init
	 * @param string
	 * @param Riot.observable
	 */
	constructor(url , storageKey)
	{
		super();

		this.initObservable();
		this.url = url;
		this._key = storageKey || 'demo-data';
	}

	/**
	 * a new hook to use later
	 */
	set store(arr)
	{
		this.setData(this._key , arr);
	}

	/**
	 * a getter method to pair with above
	 */
	get getStore()
	{
		return this.getData(this._key);
	}

	/**
	 * wrapper method allow easily switching between different libraries
	 */
	initObservable()
	{
		return riot.observable(this);
		// this.evtSrv = new NBEventService();
		// return this.evtSrv;
	}

	/**
	 * a new top level method using the $.when to promisify our code
	 */
    execute()
	{
		return $.when(this.getStore || this._getRssAsJson()).pipe(this._filter).then((json) =>
		{
			this.trigger('feed:ready' , json);
			return json;
		});
	}


	_filter(rssFeed)
	{
		return rssFeed.filter((feed) =>
		{
			return feed.category === '<![CDATA[ui-development-es6]]>';
		});
	}

	/**
	 * fetching data from remote
	 */
	_getRss()
	{
		return $.ajax({
			url:  this.url,
			crossDomain: true
		}).promise();
	}

	/**
	 * if we pipe chain the original $.ajax method, the `this` is jQuery itself
	 * which is not good. We chain if further down to access this `this`
	 * besides we can keep the code clean
	 */
	_getRssAsJson()
	{
		return this._getRss()
				   .pipe(
					   this._rssToJson
				   ).pipe((json) => {
					   	this.store = json;
						return json;
				   });
	}

	/**
	 * transform RSS data to JSON data
	 * @param xml document
	 * @param function
	 * @return array
	 */
	_rssToJson(rssFeed)
	{
		var rssJson = [];
		$(rssFeed).find('rss > channel > item').each(function()
		{
			let item = {};
			$(this).children().each(function()
			{
				var _item = $(this);
				item[_item[0].tagName] = _item.html();
			});
			rssJson.push(item);
		});
		return rssJson;
	}
}
