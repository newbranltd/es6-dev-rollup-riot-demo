'use strict';
/**
 * Utils mixin
 * src/app/mixins/utils.js
 */
export default {
    init: function()
    {
        this.on('updated', (e) =>
        {
            // console.log('validator Updated!' , e);
        });
    },
    /**
     * strip out the <![CDATA[ ]]> if any
     * @param string
     * @return string
     */
    stripCdata: function(entry)
    {

        return entry.replace('<![CDATA[','').replace(']]>','');

        // the regex version not working consistently
        // let matches = /\<\!\[CDATA\[(.*)\]\]\>/.exec(entry);
        // return matches ? matches[1] : entry;
    },
    /**
     * strip out all HTML tags
     */
    stripHtml: function(entry)
    {
        return entry.replace(/<(?:.|\n)*?>/gm, '');
    }
};
