'use strict';

export default {

	init: function()
	{
		this.on('updated', (e) =>
		{
			// console.log('validator Updated!' , e);
		});
	},

	/**
	 * from stackoverflow http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
	 */
	isEmail: function(address)
	{
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(address);
	},

	/**
	 * from css-tricks https://css-tricks.com/snippets/javascript/strip-whitespace-from-string/
	 */
	notEmpty: function(str)
	{
		if (str!==undefined && str!==null) {
			str = str + '';
			str = str.trim();
		}
		return str!=='';
	}

};
