'use strict';
/**
 * Paginator mixin
 * src/app/mixins/paginator.js
 */
export default {
    // init variables
    pages: [],
    total: 0,
    totalPages: 0,
    first: true,
    last: false,
    lastPage: null,
    nextPage: null,
    // mixin init method
    init: function()
    {
        // provide init method
    },
    /**
     * top level method
     */
    calculate: function(current)
    {
        this.current = current || 1;
        this._getBase();
        this._getNextLast();
        this._getList();
    },
    /**
     * calculate the total and pages etc
     */
    _getBase: function()
    {
        this.pages = []; // reset!
        // we don't need to pass the feeds here
        this.total = this.feeds.length;
        this.totalPages = Math.ceil(this.total/this.pageSize);
        // create an array to hold all the pages
        for (let i=0; i<this.totalPages; ++i) {
            this.pages.push(i+1);
        }
    },
    /**
     * get next and last
     */
    _getNextLast: function()
    {
        this._getNext();
        this._getLast();
    },
    /**
     * get next (first , nextPage)
     */
    _getNext: function()
    {
        this.first = (this.pages[0]===this.current);
        this.nextPage = this.current + 1;
        if (this.nextPage >= this.totalPages) {
            this.nextPage = this.totalPages - 1;
        }
    },
    /**
     * get last (last , lastPage)
     */
    _getLast: function()
    {
        this.last = (this.pages[this.totalPages-1] === this.current);
        this.lastPage = this.current - 1;
        if (this.current < 1) {
            this.current = 1;
        }
    },
    /**
     * get the list of articles to display based on the current
     */
    _getList: function()
    {
        
    }
};
