<navigation>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">UI Dev with ES6+</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class={active: is.home}><a href="#/">Home</a></li>
            <li class={active: is.about}><a href="#/about">About</a></li>
            <li class={active: is.contact}><a href="#/contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
				  Resources <span class="caret"></span>
			  </a>
              <ul class="dropdown-menu">
                <li each={opts.dropdownMenu}>
					<a href="{url}">{name}</a>
				</li>
              </ul>
            </li>
          </ul>
		  <ul class="nav navbar-nav navbar-right">
			  <li><a>V: {opts.version}</a></li>
		  </ul>
        </div>
      </div>
    </nav>

	<script>

		this.state = opts.state;
		this.is = {home: false , about: false , contact: false};

		this.state.on('route:changed' , (routeName) =>
		{
			if (this.is[routeName] !== undefined) {
				for (let r in this.is) {
					this.is[r] = (r === routeName);
				}
			}
			// this is the key
			this.update();
		});
	</script>

</navigation>
