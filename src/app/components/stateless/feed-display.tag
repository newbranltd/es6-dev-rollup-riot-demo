<feed-display>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>{title}</h3>
		</div>

		<div class="panel-body">
  		</div>

		<div class="panel-footer">
			{pubDate}
		</div>
	</div>

	<script>

		this.mixin('Utils');

		this.on('mount' , () =>
		{

			$('.panel-body' , this.root).html(
				this.stripHtml(this.stripCdata(this.opts.feed.description))
			);
			this.update({
				title: this.stripCdata(this.opts.feed.title),
				pubDate: this.opts.feed.pubDate
			});
		});
	</script>

</feed-display>
