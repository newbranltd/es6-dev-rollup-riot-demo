<jumbotron>
    <div class="jumbotron">
        <h1>{opts.title}</h1>
        <yield />
    </div>
</jumbotron>
