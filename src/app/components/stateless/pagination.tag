<pagination>
	<nav>
		<ul class="pagination">
			<li class="{disabled: first}">
	  			<a href="#/home/{lastPage}" aria-label="Previous">
	    			<span aria-hidden="true">&laquo;</span>
	  			</a>
			</li>

			<li each="{page in pages}" class="{active: page == current}">
				<a href="#/home/{page}">{page}</a>
			</li>

			<li class="{disabled: last}">
	  			<a href="#/home/{nextPage}" aria-label="Next">
	    			<span aria-hidden="true">&raquo;</span>
	  			</a>
			</li>
		</ul>
	</nav>

	<script>

		this.on('updated' , () =>
		{
			console.log(this);
		});


		// set the page size
		this.pageSize = 5;
		// import the paginator mixin
		this.mixin('Paginator');

		/**
		 * we pass over the calculate from the parent
		 */
		this.on('mount' , () =>
		{
			
			this.calculate();
			this.update();
		});
	</script>
</pagination>
