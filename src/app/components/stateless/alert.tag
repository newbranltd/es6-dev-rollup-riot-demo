<alert>
	<div class={alert:true, 'alert-warning': opts.warning , 'alert-danger': opts.danger}>
		<yield />
	</div> 
</alert>
