import '../stateless/alert.tag';
<contact>

	<h1>Contact</h1>

	<!-- we are going to create a contact form here -->

	<form name="contactForm" onsubmit={ submit } novalidate>

  		<div class={'form-group': true , 'has-success' : name.$success , 'has-error': name.$error}>
			<label for="name">Name</label>

			<input type="text" class="form-control" id="name" name="name" placeholder="Name" required />

			<alert warning="true" show={name.$error}>
				<p>
					<strong>Name is required field</strong>
				</p>
			</alert>
		</div>

		<div class={'form-group': true , 'has-success' : email.$success , 'has-error': email.$error}>
    		<label for="email">Email address</label>
    		<input type="email" class="form-control" id="email" name="email" placeholder="Email" required onblur={ blurTest } />

			<alert warning="true" show={email.$error}>
				<p>
					<strong>Email is required field</strong>
				</p>
			</alert>
		</div>

		<div class={'form-group': true , 'has-success' : message.$success , 'has-error': message.$error}>

			<label for="message">Message</label>
			<textarea class="form-control" rows="3" id="message" name="message" required></textarea>

			<alert warning="true" show={message.$error}>
				<p>
					<strong>Name is required field</strong>
				</p>
			</alert>
		</div>

		<div>
  			<button type="submit" class="btn btn-default">Submit</button>
		</div>

	</form>

	<script>
		// loading our validator mixin
		this.mixin('Validator');

		const blurTest = function()
		{
			console.log(this.email.value);
		};


		/**
		 * using loop instead of typing them one by one
		 */
		let fields = ['name' , 'email' , 'message'];
		/**
		 * clear all the $error / $success
		 */
		const clearFields = () =>
		{
			for (let field of fields) {
				this[field].$error = false;
				this[field].$success = false;
			}
		};
		/**
		 * form submit action handler
		 */
		this.submit = (e) =>
		{
			clearFields();
			for (let field of fields) {
				let value = this[field].value;
				this[field].$error = !this.notEmpty(value);
				// special case for email
				if (field==='email' && !this.email.$error) {
					this.email.$error = !this.isEmail(value);
				}
				this[field].$success = !this[field].$error;
			}
			return false;
		};

	</script>

</contact>
