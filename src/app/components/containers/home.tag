import '../stateless/jumbotron.tag';
import Feed from '../../models/feed.js';
import '../stateless/feed-display.tag';
import '../stateless/pagination.tag';
<home>
	<jumbotron title="UI Development with ES6 Javascript and Riot.js">
		<p>
    		This is the example App we built with Riot.js and ES6 Javascript.
    	</p>
	</jumbotron>

	<pagination current="{ currentPage }"></pagination>

	<feed-display each="{ paginatedFeeds }" feed="{ this }" no-reorder />

	<script>
		// init our things here
		let feedModel = new Feed('/proxy/rss/');

		feedModel.on('feed:ready' , (json) =>
		{
			this.update({feeds: json});
		});

		// execute on mount
		this.on('mount' , () =>
		{
			feedModel.execute();
			this.currentPage = 1;
		});

		const subRoute = riot.route.create(); // create another routing context
		// run the routing group
		subRoute('/home/*', (currentPage) =>
		{
			this.currentPage = currentPage;
		});

	</script>
</home>
