import readme from '../../../../README.md';
import '../stateless/display.tag';
<about>

	<display>
		<h1>About</h1>
	</display>

	<span id="displayMarkdownContent"></span>

	<script>
		this.on('mount' , () =>
		{
			$('#displayMarkdownContent' , this.root).html(markdown.toHTML(readme));
		});
	</script>
	<style>

	</style>
</about>
