/* import tag */
import './containers/home.tag';
import './containers/about.tag';
import './containers/contact.tag';
import './stateless/not-found.tag';
/* import mixin */
import Paginator from '../mixins/paginator';
import Utils from '../mixins/utils';
import Validator from '../mixins/validator';

<app>
	<div class="container" id="main"></div>

	<script>
		/* loading our mixin */
		riot.mixin('Paginator', Paginator);
		riot.mixin('Utils', Utils);
		riot.mixin('Validator', Validator);

		// router
		const MAIN_ID = '#main';
		riot.route((action) =>
		{
			action = action || 'home';
			opts.state.trigger('route:changed' , action);
		    switch (action) {
		        case 'contact':
		            riot.mount(MAIN_ID, 'contact');
		        break;
		        case 'about':
		            riot.mount(MAIN_ID , 'about');
		        break;
				case 'home':
		            riot.mount(MAIN_ID , 'home');
				break;
				default:
					riot.mount(MAIN_ID , 'not-found');
		    }
		});

		riot.route.start(true);

	</script>
</app>
