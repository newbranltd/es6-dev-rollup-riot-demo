/**
 * main entry file
 */
import riot from 'riot';
import $ from 'jquery';
import Rx from 'rx';
import markdown from 'markdown';

import Immutable from 'immutable';
// create a state for two tags to communicate
const state = riot.observable({});

// mounting the navgation
import './components/navigation.tag';
import {menu} from './data.json';
import {version} from '../../package.json';
riot.mount('navigation' , {dropdownMenu: menu , version: version , state: state});

// mount the Container Component
import './components/app.tag';
riot.mount('app' , {state: state});
// -- EOF
