/**
 * Cache service using localStorage
 * NOTE: There was a very strange bug associate with this code
 * if we pass the localStorage to an internal property i.e. this.s = localStorage
 * it works - but the downstream code in the Feed class. The riot.observable.trigger stop working (no event was fire)
 */
export default class Storage {
	/**
	 * super simple cache storage
	 */
	constructor()
	{
	}

	setData(key , data)
	{
		localStorage.setItem(key , JSON.stringify(data));
		return data;
	}
	/**
	 *
	 *
	 */
	getData(key)
	{
		return JSON.parse(localStorage.getItem(key));
	}

	delData(key)
	{
		localStorage.removeItem(k);
	}

	clearData()
	{
		localStorage.clear();
	}

}
