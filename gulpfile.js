/**
 * gulpfile.js
 * for integration with rollup / riot
 */
var gulp = require('gulp');
var webserver = require('gulp-webserver');
var shell = require('gulp-shell');
// bower required tools
var bowerFiles = require('main-bower-files');
var inject = require('gulp-inject');

// build in tool
var path = require('path');
var join = path.join;
// less processor
var less = require('gulp-less');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');
var runSequence = require('run-sequence');
// eslint
var eslint = require('gulp-eslint');

/**
 * default task - call gulp and its done
 */
gulp.task('default' , ['setup' , 'watch']);

gulp.task('setup' , function(done)
{
	runSequence('rollup' ,
				'less',
				'wiredep',
				'serve',
				done);
});

/**
 * start of tasks
 */
gulp.task('serve' , function()
{
	var dir = ['dest' , 'node_modules' , 'bower_components'];
	return gulp.src(dir)
				.pipe(
					webserver({
						livereload: true,
						open: true,
						proxies: [{
				        	source: '/proxy',
				        	target: 'http://newbranch.cn'
				    	}]
					})
				);

});

gulp.task('rollup:clear' , function()
{
	return del([
        // using path.join to make sure it works cross platform
        join('dest' , 'bundle.js')
    ]);
});

gulp.task('rollup:c' , shell.task([
	'rollup -c'
]));

gulp.task('rollup' , function(done)
{
	runSequence('rollup:clear' , 'rollup:c' , done);
});

gulp.task('watch' , function()
{
	gulp.watch([
        join('src' , 'app' , 'components' , '**' , '*.tag') ,
	] , ['rollup']);

    gulp.watch([
        'bower.json' ,join('src','index.html')
	] , ['wiredep']);
    gulp.watch([
        join('src','style','style.less')
	] , ['less']);
	gulp.watch([
		join('src' , '**' , '*.js')
	] , ['lint','rollup']);
/*
	gulp.watch([
		join('src' , '**' , '*.tag')
	] , ['tag-lint']);
*/
});

/**
 * wiredep task
 */
 gulp.task('wiredep' , function()
 {
 	var depFiles = bowerFiles();

 	var pathReplace = function(filePath , dir)
 	{
 		dir = dir || '/bower_components/';
 		var fp;
 		if (typeof dir==='string') {
 			fp = filePath.replace(dir , '');
 		}
 		else {
 			dir.forEach(function(d)
 			{
 				if (filePath.indexOf(d)>-1) {
 					fp = filePath.replace(d , '');
 				}
 			});
 		}
 		var isJs = filePath.indexOf('.js') > -1;
 		if (isJs) {
 			return '<script src="' + fp + '"></script>';
 		}
 		return '<link rel="stylesheet" type="text/css" href="' + fp + '" />';
 	};

 	return gulp.src(join('src' , 'index.html'))
 		   .pipe(
 			   inject(
 				   gulp.src(depFiles, {read: false}),
 				   {
 					   name: 'bower',
 					   transform: function(filePath)
 					   {
 						   return pathReplace(filePath);
 					   }
 				   }
 			   )
 		   )
 		   .pipe(
 			   gulp.dest('./dest')
 		   );
 });

/**
 * less task
 */
gulp.task('less' , function()
{
    return gulp.src(
            join('src','style','style.less')
          ).pipe(sourcemaps.init())
          .pipe(less())
          .pipe(sourcemaps.write('./'))
          .pipe(
			  gulp.dest(
              	join('dest','css')
			  )
		  );
});

/**
 * ESLint separate the linting
 */
gulp.task('lint', function () {
     return gulp.src(['src/**/*.js','!node_modules/**','!bower_components/**'])
         .pipe(eslint({
			 parser: 'babel-eslint'
		 }))
         .pipe(eslint.format());
});
