<{{dashCase name}}>
	<span>{{name}} tag</span>
	<script>
		// your JS code
	</script>
	{{#isStateless type}}
	<style>
		/* your styling */
	</style>
	{{/isStateless}}
</{{dashCase name}}>
