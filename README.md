# UI Development with Riot.js and ES6 Javascript Demo

This demo is working example of the series of articles publish on [newbranch blog](http://newbranch.cn).

This cover setting up your dev environment with rollup.js / gulp.js / bower / npm / plop.

We are going to talk about Component based / shadow DOM UI development with Riot.js and writing code with ES6 Javascript.  

## setup

Run

     npm run setup

## Table of contents

1. [Part I : Riot.js / rolllup.js / gulp.js](http://newbranch.cn/ui-development-with-es6-javascript-riotjs-and-rollupjs-introduction/)
2. [Part II: Assets management with Bower and other third parties dependencies](http://newbranch.cn/ui-development-with-es6-javascript-part-ii-bower/)
3. [Part III: A super quick ES6 Javascript primer](http://newbranch.cn/ui-development-with-es6-javascript-part-iii-a-super-quick-es6-js-primer/)
4. [Part IV: Introducing component-based UI Development](http://newbranch.cn/ui-development-with-es6-javascript-part-iv-component-based-development/)
5. [Part V: Shadow DOM and Riot.js](http://newbranch.cn/ui-development-with-es6-javascript-part-v-shadow-dom/)
6. [Part VI: Setting up Router](http://newbranch.cn/ui-development-with-es6-javascript-part-vi-setting-up-router/)
7. [Part VII: Cross tag communication with Reactive Pattern](http://newbranch.cn/ui-development-with-es6-javascript-part-vii-cross-tag-communication-with-reactive-pattern)
8. [Part VIII: Refactoring our App with Container and Stateless Component](http://newbranch.cn/ui-development-with-es6-javascript-part-viii-container-and-stateless-component/)
9. [Part VIIII: Event handler and mixin](http://newbranch.cn/ui-development-with-es6-javascript-part-viiii-event-handler-and-mixin/)
10. [Part X: automating workflow with plop](http://newbranch.cn/ui-development-with-es6-javascript-part-x-automating-workflow-with-plop/)
11. [Part XI: Unit testing with Mocha and Chai](http://newbranch.cn/ui-development-with-riot-js-and-es6-javascript-part-xi-testing-with-mocha-and-chai/)
12. [Part XII: Reactor pattern part 1](http://newbranch.cn/ui-development-with-riot-js-and-es6-javascript-part-xii-reactor-pattern-part-1/)
13. [Part XIII: Reactor pattern part 2](http://newbranch.cn/ui-development-with-riot-js-and-es6-javascript-part-xii-reactor-pattern-part-2/)
14. Part XIIII: Reactor pattern part 3
15. Part XV: Fluxify Riot
16. Isomorphic Riot app with Panes.js


[Joel Chu 2016](http://joelchu.com)

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/80x15.png" />
</a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">UI Development with ES6 Javascript</span>
by
<a xmlns:cc="http://creativecommons.org/ns#" href="http://newbranch.cn/ui-development-with-es6-javascript-riotjs-and-rollupjs-introduction/" property="cc:attributionName" rel="cc:attributionURL">Joel Chu</a> is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>
