/**
 * karma config for riot
 */
module.exports = function(config) {
   config.set({
     	basePath: '',
     	frameworks: ['mocha','chai','riot'],
     	plugins: [
       		'karma-mocha',
       		'karma-mocha-reporter',
       		'karma-chrome-launcher',
       		'karma-chai',
       		'karma-riot',
	   		'karma-babel-preprocessor'
     	],
     	files: [
			'node_modules/babel-polyfill/dist/polyfill.js',
       		'node_modules/chai/lib/chai.js',
       		'src/**/*.tag',
       		'test/**/*.js'
     	],
     	preprocessors: {
       		'**/*.tag': ['riot' , 'babel'],
			'**/*.js': ['babel']
	 	},
	 	babelPreprocessor: {
     		options: {
        		presets: ['es2015'],
        		sourceMap: 'inline'
      		},
      		filename: function (file) {
        		return file.originalPath.replace(/\.js$/, '.es5.js');
      		},
      		sourceFileName: function (file) {
        		return file.originalPath;
      		}
  	 	},
     	browsers: ['Chrome'],
     	reporters: ['mocha'],
     	failOnEmptyTestSuite: false,
     	singleRun: true
   })
};
