/**
 * rollup.config.js
 */
import json from 'rollup-plugin-json';
import babel from 'rollup-plugin-babel';
import riot from 'rollup-plugin-riot';
import npm from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
// for import .md file https://github.com/TrySound/rollup-plugin-string
import string from 'rollup-plugin-string';
// import eslint from 'rollup-plugin-eslint';

export default {
	entry: 'src/app/bootstrap.js',
	dest: 'dest/bundle.js',
	format: 'iife',
	plugins: [
		/*
		eslint({
			exclude: [
				'node_modules/**',
				'bower_components/**',
				'* * / *.{md,json}'
			],
			envs: ["browser", "mocha"],
			parser: "eslint-plugin-riot"  "babel-eslint"
		}),
		*/
		json(),
		string({
            extensions: ['.md']
        }),
		riot(),
		npm({
			jsnext: true,
			main: true,
			browser: true
		}),
		commonjs(),
		babel()
	],
	globals: {
    	riot: 'riot',
    	jquery: '$',
		rx: 'Rx',
		markdown: 'markdown',
		immutable: 'Immutable'
  	},
	external: [
		'riot',
		'jquery',
		'rx',
		'markdown',
		'immutable'
	]
}
